'use strict';

const render = require('./render');

const tiers = [
   {
        distance: 0, radius: 170,
        users: [
            'linux'
        ],
    },
    {
        distance: 200, radius: 64,
        users: [
            //most used technologies
            'nodejs',
            'javascript',
            'npm',
            'github',
            'gitlab',
            {url: 'https://seeklogo.com/images/Y/yarn-logo-F5E7A65FA2-seeklogo.com.png'},
            'vue-js',
            {url: "https://img.icons8.com/ultraviolet/1000/react.png"},
        ],
    },

  {
        distance: 330, radius: 58,
        users: [
            //tools
            {url: 'https://cdn.freebiesupply.com/logos/large/2x/webstorm-icon-logo-png-transparent.png'},
            'chromium',
            'console',
            'discord-logo',
            'skype--v1',
            'telegram-app',
            'vlc',
            'firefox',
            'intellij-idea',
            'amazon-web-services',
            'docker',
            'android-os',
        ],
    },

    {
        distance: 450, radius: 50,
        users: [
            //etc
            {url: 'https://vuetifyjs.com/apple-touch-icon-180x180.png'},
            'material-ui',
            'java-coffee-cup-logo',
            'kotlin',
            'sass-avatar',
            {url: 'https://ohmyz.sh/img/OMZLogo_BnW.png'},
            'counter-strike',
            'ethereum',
            'twitter',
            'youtube-play',
            'kubernetes',
            'ssh',
            {url: 'https://seeklogo.com/images/N/nginx-logo-FF65602A76-seeklogo.com.png'},
            'soundcloud--v1',
            'cloudflare',
            'html-5',
            'css3',
            {url: 'https://upload.wikimedia.org/wikipedia/commons/8/8d/Shadowsocks_logo.png'},
        ]
    },
];

async function main(){
    render(tiers)
}
main();

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

function sleep(arg){
    return new Promise(resolve => {
        setTimeout(function () {
            resolve()
        }, Number(arg * 0));
    })
}

console.log('eof');